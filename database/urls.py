from django.urls import path

from . import views

urlpatterns = [
    path('', views.index, name='index'),
    path('all', views.all, name='all'),
    path('psr/<jname>', views.psr, name='psr'),
    path('convert_atnf', views.convert_atnf, name='convert_atnf'),
    path('populate', views.populate, name='populate'),
    path('populate_observed', views.populate_observed, name='populate_observed'),
    path('fix_db', views.fix_db, name='fix_db'),
    path('list', views.list, name='list'),
    path('calculate', views.calculate, name='calculate'),
    path('check', views.check, name='check'),
]
