import os
import json

import numpy as np


class PulsarATNF:

    def __init__(self):
        self.PSRJ = None
        self.PSRB = None
        self.P0 = None
        self.P1 = None
        self.F0 = None
        self.F1 = None
        self.S400 = None
        self.S1400 = None
        self.DM = None
        self.SURVEY = None
        self.TYPE = None
        self.DECJ = None
        self.RAJ = None


class ATNF:

    def __init__(self):
        self.records = []

    def save_data(self, out_file):
        s = json.dumps(self, cls=ATNFEncoder, indent=4, sort_keys=True)
        f = open(out_file, 'w')
        f.write(s)
        f.close()

    @staticmethod
    def load(file_name):
        f = open(file_name)
        pu = json.load(f, cls=ATNFDecoder)
        f.close()
        return pu

    @staticmethod
    def convert_db(in_file, out_file):
        """

        Converts ATNF db file to json

        Args:
            in_file: input filename
            out_file: output filename

        Returns:

        """
        psrs = ATNF()
        f = open(in_file)
        lines = f.readlines()
        p = PulsarATNF()
        fields = dir(p)
        # skip first 5 lines
        for i in range(5, len(lines)):
            if lines[i].startswith('@-'):
                psrs.records.append(p)
                p = PulsarATNF()
            else:
                res = lines[i].split()
                if res[0] in fields:
                    setattr(p, res[0], res[1:])
        f.close()
        # complete PO, F1 records
        for p in psrs.records:
            if p.P0 is None and p.F0 is not None:
                p.P0 = [str(1. / float(p.F0[0]))]
            elif p.P0 is not None and p.F0 is None:
                p.F0 = [str(1. / float(p.P0[0]))]
            if p.P1 is None and p.F1 is not None:
                p.P1 = [str(-float(p.F0[0]) ** -2. * float(p.F1[0]))]
            elif p.P1 is not None and p.F1 is None:
                p.F1 = [str(-float(p.P0[0]) ** -2 * float(p.P1[0]))]
        psrs.save_data(out_file)


class ATNFDecoder(json.JSONDecoder):

    def decode(self, json_string):
        """
        :param json_string: is basically string that you give to json.loads method
        """
        obj = json.loads(json_string)
        pu = ATNF()
        for key, value in obj.items():
            setattr(pu, key, value)
        return pu


class ATNFEncoder(json.JSONEncoder):

    def default(self, o):
        if isinstance(o, np.ndarray):
            if o.ndim == 1:
                return o.tolist()
            else:
                return [self.default(o[i]) for i in range(o.shape[0])]
        elif isinstance(o, (complex, np.complex)):
            return [o.real, o.imag]
        else:
            return o.__dict__
