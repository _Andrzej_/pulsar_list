# Creates already observed pulsar list
import os
import json
import glob
from datetime import datetime

import numpy as np

class Pulsar:
    def __init__(self, jname):
        self.jname = jname
        self.observations = []

class Observation:
    def __init__(self, date, freq, duration):
        self.date = date
        self.freq = freq
        self.duration = duration

class PulsarEncoder(json.JSONEncoder):
    def default(self, o):
        if isinstance(o, np.ndarray):
            if o.ndim == 1:
                return o.tolist()
            else:
                return [self.default(o[i]) for i in range(o.shape[0])]
        elif isinstance(o, (complex, np.complex)):
            return [o.real, o.imag]
        else:
            return o.__dict__


def create_list(datadir="/fred/oz005/search/"):
    pulsars = []
    dateformat = "%Y-%m-%d-%H:%M:%S"
    psrfolders = filter(os.path.isdir, glob.glob(datadir+"/*"))
    for psr in psrfolders:
        jname = psr.split("/")[-1]
        print(jname)
        p = Pulsar(jname)
        sessions = filter(os.path.isdir, glob.glob(psr+"/*"))
        for ses in sessions:
            date = ses.split("/")[-1]
            print("\t{}".format(date))
            frequencies = filter(os.path.isdir, glob.glob(ses+"/*"))
            for freq in frequencies:
                fre = freq.split("/")[-1]
                print("\t\t{}".format(fre))
                files = sorted(glob.glob(freq+"/*sf"))
                if len(files) > 0:
                    st = datetime.strptime(files[0].split("/")[-1].split(".")[0], dateformat)
                    end = datetime.strptime(files[-1].split("/")[-1].split(".")[0], dateformat)
                    dt = end - st
                    p.observations.append(Observation(date, fre, dt.total_seconds()))
        pulsars.append(p)
    f = open("../static/database/observed.json", "w")
    json.dump(pulsars, f, cls=PulsarEncoder, indent=4, sort_keys=True)
    f.close()
    return

    #print(os.listdir(datadir))

def main():
    create_list()
    print("Bye")


if __name__ == "__main__":
    main()
