# Creates processed pulsar list
import os
import sys
import json
import glob
import shutil as sh
import subprocess
from datetime import datetime

import numpy as np

class Pulsar:
    def __init__(self, jname):
        self.jname = jname
        self.observations = []

class Observation:
    def __init__(self, date, freq, plots):
        self.date = date
        self.freq = freq
        self.plots = plots

class PulsarEncoder(json.JSONEncoder):
    def default(self, o):
        if isinstance(o, np.ndarray):
            if o.ndim == 1:
                return o.tolist()
            else:
                return [self.default(o[i]) for i in range(o.shape[0])]
        elif isinstance(o, (complex, np.complex)):
            return [o.real, o.imag]
        else:
            return o.__dict__


def create_list(datadir="/fred/oz005/users/aszary/search_processed/"):
    pulsars = []
    dateformat = "%Y-%m-%d-%H:%M:%S"
    psrfolders = filter(os.path.isdir, glob.glob(datadir+"/J*"))
    for psr in psrfolders:
        jname = psr.split("/")[-1]
        print(jname)
        p = Pulsar(jname)
        sessions = filter(os.path.isdir, glob.glob(psr+"/*"))
        for ses in sessions:
            date = ses.split("/")[-1]
            print("\t{}".format(date))
            frequencies = filter(os.path.isdir, glob.glob(ses+"/*"))
            for freq in frequencies:
                fre = freq.split("/")[-1]
                print("\t\t{}".format(fre))
                plots = sorted(glob.glob(freq+"/plots/*png"))
                if len(plots) == 0:
                    print("Error! No plots here...")

                p.observations.append(Observation(date, fre, plots))
        pulsars.append(p)
    f = open("../static/database/processed.json", "w")
    json.dump(pulsars, f, cls=PulsarEncoder, indent=4, sort_keys=True)
    f.close()
    return


def copy_plots(outdir, jsonfile="../static/database/processed.json", regex=["lrfs2", "p3evo", ".spCF.I_zoom"]):
    f = open(jsonfile)
    pulsars = json.load(f)
    f.close()

    for pulsar in pulsars:
        jname = pulsar["jname"]

        for obs in pulsar["observations"]:
            plots = []
            for plot in obs["plots"]:
                for reg in regex:
                    if reg in plot:
                        plots.append(plot)

            for plot in plots:
                fn = plot.split("/")[-1]
                outfile = os.path.join(outdir, "{}_{}".format(jname, fn))
                sh.copyfile(plot, outfile)

            if len(plots) == 0:
                print("# WARNING: No plots for {} ({})".format(jname, obs["date"]))


def view_plots(dirname, start=0):
    files = sorted(glob.glob(dirname+"J*"))
    #print(files)
    size = len(files)
    err = open("err.txt", "w")
    for i in range(start, size, 1):
        print("{} {}/{}".format(files[i].split("/")[-1], i, size))
        #os.system("okular {}".format(file))
        subprocess.call(["okular", files[i]], stderr=err)
    err.close()

def main():
    argv = sys.argv
    if len(argv) > 1:
        start = int(argv[1])
    else:
        start = 0

    #create_list()
    #copy_plots("/fred/oz005/users/aszary/plots/")
    view_plots("/home/szary/work/MeerTime/plots/", start=start)
    print("Bye")


if __name__ == "__main__":
    main()
