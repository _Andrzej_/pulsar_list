
def dms2float(dms, sep=":"):
    res = dms.split(sep)
    fl = 0.
    for i in range(len(res)):
        fl += float(res[i]) / 60 ** i
    return fl


def dms2err(dms, err, sep=":"):
    # check if err is number
    try:
        err = float(err)
    except:
        err = 1 # an order of magnitude error will be used
    res = dms.split(sep)
    # degrees/hours minutes or seconds
    le = len(res)
    if le == 1:
        ord = 60 * 60
        er = float(err) * ord
    elif le == 2:
        ord = 60
        er = float(err) * ord
    elif le == 3:
        ord = 1
        pos = len(res[2])
        if pos == 2:
            mod = ord
        else:
            mod = 10 ** (3 - pos)
        er = float(err) * mod
    return er


def edot(p0, p1):
    return 3.95e31 * float(p1) / 1e-15 * float(p0) ** -3

def bsurf(p0, p1):
    return 3.2e19 * float(p1) ** 0.5 * float(p0) ** 0.5

def rms(dt, t_sys=15, gain=2.8, Na=64, Np=2, dnu=770):
    """
    t_sys = 15-20 K, gain = 2.8 K/Jy, bandwidth = 770 MHz (for 64 antenas)
    """
    return t_sys / (gain * (Na*(Na-1) * Np * dt * dnu) ** 0.5)

def s_min(dt, t_sys=15, gain=2.8, Na=64, Np=2, dnu=770):
    """
    The minimum detectable flux density in mJy
    """
    return 5 * rms(dt, t_sys, gain, Na, Np, dnu)
