import json
from datetime import datetime

from django.shortcuts import render, get_object_or_404
from django.http import HttpResponse
from django.db.models import F
from django.template import loader

from .models import Pulsar, Observation
from .modules.atnf import ATNF
from .modules.fun import *

def index(request):
    pulsars = Pulsar.objects.filter(observations__pulse_number__gte=0).distinct().order_by('PSRJ')
    template = loader.get_template("database/index.html")
    contex = {"pulsars": pulsars, "number": len(pulsars), }
    return HttpResponse(template.render(contex, request))


def all(request):
    pulsars = Pulsar.objects.all().order_by('PSRJ')
    template = loader.get_template("database/index.html")
    contex = {"pulsars": pulsars, "number": len(pulsars), }
    return HttpResponse(template.render(contex, request))


def psr(request, jname):
    pulsar = get_object_or_404(Pulsar, PSRJ=jname)
    template = loader.get_template("database/psr.html")
    contex = {"pulsar": pulsar, }
    return HttpResponse(template.render(contex, request))


def convert_atnf(request):
    ATNF.convert_db("database/static/database/psrcat_1_62.db", "database/static/database/atnf.json")
    return HttpResponse("Converts ATNF db")
    #return HttpResponse("Convert ATNF db? Answer first to the Ultimate Question of Life, the Universe, and Everything. ")

def populate(request):
    pulsars = ATNF.load("database/static/database/atnf.json").records
    res = "Number of pulsars in atnf.json file: {}<br />".format(len(pulsars))
    added = []
    updated = []
    for i,pulsar in enumerate(pulsars):
        psrj = pulsar["PSRJ"][0]
        rec = Pulsar.objects.filter(PSRJ=psrj)
        # add new record
        if len(rec) == 0:
            pu = Pulsar(PSRJ=psrj)
            added.append("{}<br />".format(psrj))
        elif len(rec) == 1:
            pu = rec[0]
            updated.append("{}<br />".format(psrj))
        else:
            continue
        print(psrj)
        for key, value in pulsar.items():
            if value is not None:
                setattr(pu, key, value[0])
                if key == "DECJ":
                    pu.decj_float = dms2float(value[0])
                    # incomplete database problem
                    try:
                        pu.decj_err = dms2err(value[0], value[1])
                    except IndexError:
                        pu.decj_err = dms2err(value[0], 1.)
                if key == "RAJ":
                    pu.raj_float = dms2float(value[0])
                    pu.raj_err = dms2err(value[0], value[1])
                #print("\t{} {}".format(key, value))
        if pu.P0 is not None and pu.P1 is not None:
            #print("{} {}".format(pu.P0, pu.P1))
            if float(pu.P1) > 0.:
                pu.edot = edot(pu.P0, pu.P1)
                pu.bsurf = bsurf(pu.P0, pu.P1)
        pu.save() # TODO uncomment
        #break
    res += "Added: {}<br />".format(added)
    res += "Updated: {}<br />".format(updated)
    return HttpResponse("Populates pulsar db ...<br />{}".format(res))

def populate_observed(request):
    dateformat = "%Y-%m-%d-%H:%M:%S"
    res = ""
    f = open("database/static/database/observed.json")
    pulsars = json.load(f)
    f.close()
    res += "Pulsar number: {} <br />".format(len(pulsars))
    for pulsar in pulsars:
        print(pulsar["jname"])
        # exceptions

        if pulsar["jname"] == "J0125-2327": pulsar["jname"] = "J0125-23"
        if pulsar["jname"] == "J0955-6150": pulsar["jname"] = "J0955-61"
        if pulsar["jname"] == "J2140-2310": pulsar["jname"] = "J2140-2310A"
        if pulsar["jname"] == "": pulsar["jname"] = ""
        if pulsar["jname"] == "": pulsar["jname"] = ""
        if pulsar["jname"] != "J0000-0000" and  pulsar["jname"] !="J1939-6342_S":
            pu = Pulsar.objects.get(PSRJ=pulsar["jname"])
            changed = False
            for obs in pulsar["observations"]:
                dt = datetime.strptime(obs["date"], dateformat)
                ob = pu.observations.filter(datetime=dt)
                if len(ob) == 0:
                    changed = True
                    pulse_number = int(float(obs["duration"]) / float(pu.P0))
                    o = Observation(datetime=dt, freq=float(obs["freq"]), duration=obs["duration"], pulse_number=pulse_number)
                    o.save()
                    pu.observations.add(o)
                    res += "{} <br />".format(o)
            if changed is True:
                pu.save()
    #res += "{}".format(pulsars)
    return HttpResponse("Populates observed pulsar db ...<br />{}".format(res))


def fix_db(request):
    # addding coordinates from .txt file why? RAJ DECJ only for some (~300) sources
    # sed '/^$/d' cordinates.txt -> removes empty lines  # use mattpitkin/psrqpy for God's sake
    f = open("database/static/database/coordinates_1_62.txt")
    lines = f.readlines()
    f.close()
    recs = Pulsar.objects.filter(DECJ=None)  # Declination below 20
    for rec in recs:
        for line in lines:
            if rec.PSRJ in line:
                sp = line.split()
                rec.DECJ = sp[6]
                rec.RAJ = sp[3]
                rec.decj_float = dms2float(sp[6])
                rec.raj_float = dms2float(sp[3])
                rec.decj_err = dms2err(sp[6], sp[7])
                rec.raj_err = dms2err(sp[3], sp[4])
                break
        rec.save()
    return HttpResponse("Fixing db records...<br />")


def list(request):
    res = ""

    #recs = Pulsar.objects.filter(decj_float__lte=20.)  # Declination below 20
    #recs = recs.filter(P0__gte=0.01) # no millisecond pulsars
    #recs = recs.filter(decj_err__lte=60) # 1 arcmin is enough?
    #recs = recs.exclude(observations__pulse_number__gte=0).distinct()
    #recs = recs.filter(s_min__gte=2.*F("S1400"))
    #recs = recs.filter(decj_err__lte=60) # 1 arcmin is enough?

    recs = Pulsar.objects.filter(observations__pulse_number__gte=0).distinct()
    recs = recs.filter(P0__gte=0.01) # no millisecond pulsars
    recs = recs.filter(edot__lte=5e32) # low Edot pulsars

    #recs = recs.filter(decj_float__gte=20.)

    #recs = Pulsar.objects.filter(DECJ=None)
    pulse_number = 1024
    t = 0  # time required
    for rec in recs:
        #res += "{} {} {}<br /> ".format(rec.PSRJ, rec.P0, rec.s_min)
        res += "{} <br /> ".format(rec.PSRJ)
        t += rec.P0 * pulse_number
    # in hours
    t = t / (60 * 60)
    res = "Time: {}h <br /> Pulsar list:<br /> {} <br /> {}".format(t,len(recs), res)

    return HttpResponse("{}".format(res))
    #return HttpResponse(content_type="application/force-download") # "{}".format(res))


def calculate(request):
    res = ""
    pulsars = Pulsar.objects.filter(S1400__gt=0)
    pulsars = Pulsar.objects.filter(P0__gt=0).distinct()
    #pulsars = Pulsar.objects.filter(P0__gt=0, S1400__gt=0, observations__pulse_number__gte=0).distinct()
    res += "{} <br />".format(len(pulsars))

    bins = 1024
    for pulsar in pulsars:
        dt = float(pulsar.P0) / bins
        m_rms = rms(dt)
        smin = s_min(dt)
        pulsar.m_rms = m_rms
        pulsar.s_min = smin
        res += "{} {} {} <br />".format(pulsar.PSRJ, smin, pulsar.S1400)
        pulsar.save()


    return HttpResponse("Calculates some values <br />{}".format(res))
    #return HttpResponse(content_type="application/force-download") # "{}".format(res))


def check(request):
    res = ""
    f = open("database/static/database/meertime_tpa_20191028.csv")
    #f = open("database/static/database/meertime_tpa_20191017_3.csv")
    for line in f.readlines():
        re = line.split(",")
        jname = re[0]
        dt = float(re[1])
        pu = Pulsar.objects.get(PSRJ=jname)
        p0 = float(pu.P0)
        pulse_number = int(dt / p0)
        res += "{} <b>{}</b> P0:{}<br />".format(jname, pulse_number, pu.P0)
    f.close()
    return HttpResponse("{}".format(res))
    #return HttpResponse(content_type="application/force-download") # "{}".format(res))
