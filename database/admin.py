from django.contrib import admin

# Register your models here.

from .models import Pulsar, Observation

admin.site.register(Pulsar)
admin.site.register(Observation)
